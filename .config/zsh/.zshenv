export ZDOTDIR=~/.config/zsh
export HISTFILE=$ZDOTDIR/.zsh_history

# Secrets (files with secrets int them)
# make them with mk-secrets
SECRETS_DIR=/tmp/$(id -u)-secrets
SECRETS=(
  ANSIBLE_VAULT_PASSWORD_FILE
  FOREMAN_SSL_KEY
  FOREMAN_SSL_CERT
)
for secret in ${SECRETS[@]}; do
  export ${secret}=${SECRETS_DIR}/${secret}
done

# "work" prefix
[[ -d $HOME/private/work ]] && WORK_PREFIX=$HOME/private/work
[[ -d $HOME/work ]] && WORK_PREFIX=$HOME/work

# these next few only make sense if $WORK_PREFIX exists
if [[ ! -z $WORK_PREFIX ]]; then
    # work bin dir
    PATH=$WORK_PREFIX/bin:$PATH
    
    # LaTeX stuff
    [[ -f $WORK_PREFIX/LaTeX/texinputs ]] && export TEXINPUTS=".:$WORK_PREFIX/LaTeX/texinputs:"
    [[ -f $WORK_PREFIX/LaTeX/bib ]] && export BIBINPUTS=".:$WORK_PREFIX/LaTeX/bib:"
    [[ -f $WORK_PREFIX/LaTeX/bst ]] && export BSTINPUTS=".:$WORK_PREFIX/LaTeX/bst:"

    # SAC - Seismic Analysis Code
    if [ -d "$WORK_PREFIX/software/sac" ] ; then
        PATH=$WORK_PREFIX/software/sac/bin:$PATH
        export SACAUX=$WORK_PREFIX/software/sac/aux
        export SAC_DISPLAY_COPYRIGHT=0
    fi
fi

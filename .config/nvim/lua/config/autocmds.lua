-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here

-- delete trailing whitespace when writing files
vim.api.nvim_create_autocmd("BufWritePre", { pattern = "*.py,*.md,*.rst", command = "%s/\\s\\+$//e" })

#!/usr/bin/env bash

USE_SSH=${USE_SSH-false}
REPO_URL=${REPO_URL-https://gitlab.com/slloyd/dotfiles.git}
RUN_INSTALL_SCRIPTS=${RUN_INSTALL_SCRIPTS-false}
DOT_DIR=${HOME}/.dotfiles
SCRIPTS_DIR=${HOME}/.local/share/dotfiles/scripts

dots() {
  git --git-dir=${DOT_DIR} --work-tree=${HOME} $@
}

get_remote_url() {
  dots remote get-url --push origin
}

run_install_scripts() {
  for script in ${SCRIPTS_DIR}/??-install-*.sh; do
    . ${script}
  done
}

main() {
  if $USE_SSH; then
    REPO_URL=$(echo $REPO_URL | sed -e 's/https:\/\//git@/' -e 's/\//:/')
  fi
  echo "Using repository: ${REPO_URL}"

  if [ -d ${DOT_DIR} ]; then
    echo "Found existing dotfile directory!"
    if [[ $(get_remote_url) == ${REPO_URL} ]]; then
      echo "Attempting fast-forward..."
      dots pull --ff-only
    elif [[ $(get_remote_url) != ${REPO_URL} ]]; then
      echo "Repository uses different remote url: $(get_remote_url)"
      echo "Aborting!"
      exit 1
    else
      echo "Something went wrong - Aborting!"
      exit 1
    fi
  else
    git clone --bare ${REPO_URL} ${DOT_DIR}
    dots config status.showUntrackedFiles no
  fi
  dots checkout

  if ${RUN_INSTALL_SCRIPTS} ; then
    echo "running install scripts..."
    run_install_scripts
  fi
}

main

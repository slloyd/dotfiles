#!/usr/bin/env zsh
 
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

### Aliases

if [ -f ~/.config/aliases ]; then
  . ~/.config/aliases
fi

### mise-en-place (path is setup in .profile)

if command -v mise 1>/dev/null 2>&1; then
  eval "$(mise activate zsh)"
fi
 

### Zinit

if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
  print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
  command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
  command git clone https://github.com/zdharma-continuum/zinit "$HOME/.zinit/bin" && \
    print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
    print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit


zinit light-mode for \
  zdharma-continuum/zinit-annex-readurl \
  zdharma-continuum/zinit-annex-bin-gem-node \
  zdharma-continuum/zinit-annex-patch-dl \
  zdharma-continuum/zinit-annex-rust

zinit light zdharma-continuum/fast-syntax-highlighting

### End of Zinit

### Completions and functions

#  History settings
zinit snippet OMZL::history.zsh
zinit light zsh-users/zsh-history-substring-search
zinit light zsh-users/zsh-autosuggestions

# ccat and cless instead of cat and less (more) for highlighed output
# zinit ice from"gh-r" as"program"; zinit load alecthomas/chroma
# ZSH_COLORIZE_TOOL=chroma
# ZSH_COLORIZE_CHROMA_FORMATTER=terminal16m
zinit snippet OMZP::colorize
ZSH_COLORIZE_STYLE="colorful"

# Add extract function to unpack various archives
zinit snippet OMZP::extract

# Press Esc twice to run previous command with sudo
zinit snippet OMZP::sudo

# nicer vi mode
zinit ice depth=1; zinit light jeffreytse/zsh-vi-mode

# Allow SSH tab completion for mosh hostnames
zinit snippet OMZP::mosh

# Vagrant completions
zinit ice as"completion"; zinit snippet https://github.com/ohmyzsh/ohmyzsh/blob/master/plugins/vagrant/_vagrant

### User specific stuff

# Add personal functions and completions to fpath
fpath=( ~/.config/zsh/functions "${fpath[@]}" )
fpath=( ~/.config/zsh/completions "${fpath[@]}" )
autoload -Uz $fpath[1,2]/*(.:t)

if [[ ( -d ~/.config/zsh/.zshrc.d ) && ( -n "$(ls -A ~/.config/zsh/.zshrc.d)" ) ]]; then
	for rc in ~/.config/zsh/.zshrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

autoload -Uz compinit

if [[ -n $ZDOTDIR/.zcompdump(#qN.mh+24) ]]; then
  compinit
  touch $ZDOTDIR/.zcompdump
else
  compinit -C
fi
zstyle ':completion:*' menu select

### End of completions and functions


### p10k

zinit ice depth=1; zinit light romkatv/powerlevel10k

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh

### End of p10k

# vim: ft=bash

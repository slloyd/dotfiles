#!/bin/sh

set -e

BINDIR=$HOME/.local/bin

if ! command -v bw &> /dev/null; then
  echo "Installing bitwarden cli to ${BINDIR}/bw"
  curl -L "https://vault.bitwarden.com/download/?app=cli&platform=linux" -o /tmp/Z$$.zip
  if [[ ! -d $BINDIR ]]; then
    mkdir -p $BINDIR
  fi
  unzip -d $BINDIR /tmp/Z$$.zip
  chmod u+x ${BINDIR}/bw
else
  echo "Bitwarden cli version $(bw --version) already installed."
fi

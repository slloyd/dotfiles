#!/bin/sh

if command -v pyenv &> /dev/null; then
  echo "$(pyenv --version) already installed."
elif test ! -d ~/.pyenv; then
  echo -e "Downloading and running pyenv-installer\n"
  curl https://pyenv.run | bash
else
  echo "Pyenv probably already installed (found ~/.pyenv directory)."
fi
